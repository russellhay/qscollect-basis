from setuptools import setup, find_packages

setup(
   name='qscollect-basis',
   version='0.1.0ß',
   namespace_packages=['qscollect', 'qscollect.collectors', 'qscollect.collectors.contrib'],
   packages = ['qscollect.collectors.contrib'],
   url='http://bitbucket.org/russellhay/qscollect-basis',
   author='Russell Hay',
   author_email='me@russellhay.com',
   description='A qscollect collector for Basis Health Trackers',
   install_requires=[
       "qscollect>=1.1",
   ]
)
