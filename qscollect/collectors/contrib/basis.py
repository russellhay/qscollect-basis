""" Download data from app.mybasis.com and store it for later mining

  :ref basisdataexport: https://github.com/btroia/basis-data-export/blob/master/basisdataexport.php
"""

import datetime
import json
import dateutils
import logging
import pytz
import requests
import os.path as path

log = logging.getLogger("BasisCollector")

import qscollect.collector_base as collector_base

PACIFIC = pytz.timezone('US/Pacific')
UTC = pytz.utc
INTERVAL = 60  # 1 per minute?
TEST_DIR = path.abspath(
    path.join(
        path.dirname(__file__),
        "..",
        "..",
        "test_data"
    )
)
URL = "".join((
    "https://app.mybasis.com/api/v1/chart/{token}.json?",
    "summary=true",
    "&interval={interval}",
    "&units=s",
    "&start_date={start}",
    "&start_offset=0",
    "&end_offset=0",
    "&heartrate=true",
    "&steps=true",
    "&calories=true",
    "&gsr=true",
    "&skin_temp=true",
    "&air_temp=true",
    "&bodystates=true"
))
METRICS = (
    "skin_temp",
    "heartrate",
    "air_temp",
    "calories",
    "gsr",
    "steps"
)

class BasisCollector(collector_base.CollectorBase):
    """ Collects the total and the unread counts for your inbox on an imap server """
    def __init__(self, db=None):
        super(BasisCollector, self).__init__(db, "file", "basis")

    def register(self, system):
        self._system = system
        system.register_schedule(self, day=1)

    def __call__(self, config=None):
        if config is None:
            config = self.keys

        log.debug(config)
        self.token = open(config['password_path'], "r").readline().strip()

        yesterday = self._calculate_yesterday()
        if config['TEST']:
            getter = self._load_test_data
        else:
            getter = self._load_data
        data = self._get_data(yesterday, getter=getter)

        for entry in data:
            yield entry

    def _calculate_yesterday(self):
        """ Calculates what yesterday's date was, defaults to PACIFIC time """
        today = datetime.datetime.now(tz=PACIFIC)
        today.replace(hour=0, minute=0, second=1)
        delta = dateutils.relativedelta(days=1)
        yesterday = today - delta
        return yesterday

    def _get_data(self, yesterday, getter):
        """ returns entries for storing in the database """

        url = URL.format(
            token=self.token,
            interval=INTERVAL,
            start=yesterday.strftime("%Y-%m-%d")
        )
        log.debug(url)
        results = getter(url)

        start_time = datetime.datetime.fromtimestamp(int(results['starttime']), tz=PACIFIC)
        end_time = datetime.datetime.fromtimestamp(int(results['endtime']), tz=PACIFIC)
        interval = int(results['interval'])
        delta = dateutils.relativedelta(seconds=interval)

        log.debug("Start Time: {0}".format(start_time))
        log.debug("End Time: {0}".format(end_time))

        current_time = start_time
        index = 0
        while current_time < end_time:
            entry = {
                'modified': current_time.astimezone(UTC)
            }
            for metric in METRICS:
                entry[metric] = results['metrics'][metric]['values'][index]

            yield entry

            index += 1
            current_time = current_time + delta


    def _load_data(self, url):
        results = requests.get(url)
        return results.json()

    def _load_test_data(self, url):
        log.debug("Load Test Data used")
        with open(path.join(TEST_DIR, "basis.json")) as f:
            data = f.read()
            return json.loads(data)